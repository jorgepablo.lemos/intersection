import os
import math
import sys
import numpy as np
import pandas as pd
import re
import random
from functions import readPDS, ray_triangle_intersection, drawTrack, trackBright
import time
  
#################################################
#                                               #
#                  MAIN CODE                    #
#                                               #
#################################################

#%%%%%%%%%%%%%%%%%%%%%%%%%
# TODO: 
#%%%%%%%%%%%%%%%%%%%%%%%%%

# MAXIMUM NUMBER OF TRACKS PER IMAGE
NTracks = 200
formt = '%.1f','%.1f','%.1f','%.1f','%.3E','%.3E','%.3E','%.3E','%.3E','%.1f'

# GET SOLAR FLUX

start_time = time.time()

Flux = pd.read_csv('input/solar_flux.txt',sep=' ',header=0)

# LOAD IMAGE AND GET CAMERA INFORMATION ---------

if len(sys.argv) != 2:
  sys.exit('ERROR: Image file missing. Usage: python3 intersection.py <imagefile>')
elif not os.path.isfile(sys.argv[1]):
  errstr = 'ERROR: Image file ' + sys.argv[1] + ' not found'
  sys.exit(errstr)

imNameAbsolute = sys.argv[1]
imName = imNameAbsolute.split('/')[-1]

print('Reading image ',imName,'...')
imageFile = readPDS(imNameAbsolute)
minBright = np.median(imageFile.IMAGE)+np.std(imageFile.IMAGE) # minimum brightness of the track
# build triangles defining FOV
stateSC = imageFile.ACQUISITION.SC_STATE
corners = imageFile.ACQUISITION.CORNERS
tExp = imageFile.ACQUISITION.EXP_TIME
binning = imageFile.ACQUISITION.BIN
Filter = imageFile.ACQUISITION.FILTER
phAng = imageFile.ACQUISITION.PHASE_ANGLE
rh = imageFile.ACQUISITION.HELIOCENTRIC_DISTANCE
print('Done')

# FIND INTERSECTING TRAJECTORIES ---------------

print('Looping through trajectory files ...')
resultsDir = 'results' # directory with results folder for each dust size
sizePlan = pd.read_csv('input/size_plan.txt',sep='\t',header=1)
sizePlan.columns=['ID','SIZE','DENS','VIN']

for root, dirs, trajFiles in os.walk(resultsDir): # search recoursively in each directory
  if not root.split('/')[-1]=='trajectories': continue
  dustProp = int(re.findall('([0-9]+)',root)[0])
  if sizePlan.loc[sizePlan['ID'] == dustProp].SIZE.empty:
    print('Particle no. ',dustProp,' not found in size plan')
    continue
  print('Searching tracks for particle no. ',dustProp)
  dustSize = np.float64(sizePlan.loc[sizePlan['ID'] == dustProp].SIZE)
  dLim = 30.e3 # Maximum value in Frattin et al. 2021. 
  # not calculating the proper dLim cause I still don't know the length of the track generated, so the limit
  # distance could be WAY bigger than the measured one
  vertices = np.row_stack((stateSC[:3],stateSC[:3]+corners*dLim))
  triangles = np.empty((4,3,3))
  for i in range(4):
    triangles[i,:,:] = vertices[[0,i%4+1,(i+1)%4+1]]
# defining empty arrays for the track data
  track = np.empty((NTracks,4))
  trackBrightDens = np.empty((NTracks))
  partDist = np.empty((NTracks))
  timeInter = np.empty((NTracks))
  Vel = np.empty((NTracks,3))
  NT = 0
  randTrajFiles = random.sample(trajFiles,len(trajFiles)) # randomize the order in which the trajectory files are read
  for tf in randTrajFiles:
    if not tf[0:14] == 'r_trajectories': continue # omit files not containing trajectories

# load trajectories
    data = pd.read_csv(root+'/'+tf,sep='\s+',header=0)
    R = np.sqrt((data.xi-stateSC[0])**2+(data.yi-stateSC[1])**2+(data.zi-stateSC[2])**2)
    inRange = R<dLim
    data = data.assign(cameraDist=R)
    data = data.assign(inRange=inRange)
    particle = data.groupby('id') # group by particle id, loop over those groups
    for i in list(particle.groups.keys()):
      inCameraRange = particle.get_group(i)['inRange'] # check the ones inside the camera range
      if inCameraRange.any():

# check for intersections
        # load particle position and velocity vectors in the (non rotating) Cheops frame at the initial observation time
        X = np.vstack([[0.,0.,0.],particle.get_group(i)[['xi', 'yi', 'zi']].values[inCameraRange,:]])
        V = particle.get_group(i)[['vxi', 'vyi', 'vzi']].values[inCameraRange,:]
        T = particle.get_group(i)[['t']].values[inCameraRange,:]
        xInter = np.empty((2,3)) 
        vDust = np.empty((2,3))
        Rcam = np.empty((2))
        nInter = 0
        for j in range(np.shape(X)[0]-1):
          for I in range(4):
            testInter, Intersection = ray_triangle_intersection(X[j,:], X[j+1,:], triangles[I,:,:])
            if testInter: 
              vDust[nInter,:] = np.matmul(imageFile.ACQUISITION.TRANS_MATRIX,np.mean(V[j:j+2,:],axis=0)) 
              xInter[nInter,:] = np.matmul(imageFile.ACQUISITION.TRANS_MATRIX,Intersection-stateSC[:3]) # position in camera frame with (0,0) at the center of the image
              Rcam[nInter] = np.sqrt((Intersection[0]-stateSC[0])**2+(Intersection[1]-stateSC[1])**2+(Intersection[2]-stateSC[2])**2)
              nInter += 1
          if nInter == 2: # 2 intersections with the FOV: one enter, one exit
            break
        if nInter == 2:
          auxTrack = drawTrack(xInter,np.mean(vDust,axis=0),stateSC,tExp,corners,binning)
          trackLength = np.sqrt(np.power(auxTrack[2]-auxTrack[0],2)+np.power(auxTrack[3]-auxTrack[1],2))
          if trackLength < 10.: continue
          trackB = trackBright(dustSize,np.mean(Rcam),binning,Filter,phAng,rh)
          auxBright = trackB / trackLength
          if auxBright > minBright:
            track[NT,:] = auxTrack
            trackBrightDens[NT] = auxBright
            partDist[NT] = np.mean(Rcam)
            timeInter[NT] = T[j]
            Vel[NT,:] = np.mean(vDust,axis=0)
            NT += 1 # if not bright or long enough, don't save it

        if NT >= np.shape(track)[0]: break
    if NT >= np.shape(track)[0]: break

  outTrack = np.c_[track[:NT,:],trackBrightDens[:NT],partDist[:NT],Vel[:NT,:],timeInter[:NT]]
  with open(root+'/tracks_'+imName[:-4]+'.txt','w') as f:
    if NT == 0:
      f.write('No tracks found for this type of particle')
      print('No tracks found for particle ',dustProp)
    else:
      f.write('xi yi xf yf bright Rcam vx vy vz time\n')
      np.savetxt(f, outTrack,fmt = formt)
      print('Found ',NT,' tracks for particle ',dustProp)

print('Synthetic image generator completed')
print('Total elapsed time: %s seconds ' % (time.time() - start_time))

import re
import struct
import numpy as np
import os
import spiceypy as spice

##################################################
#                                                #
#                   readPDS                      #
#                                                #
#   This function takes the path of a PDS image  #
# file as an input, and extracts the header and  #
# image objects present in it. The output is a   #
# variable with one attribute per object.        #
##################################################

def readPDS(path):

  """Reads image in the Planetary Data System (PDS) format

  Parameters
  ----------
  path : string
    Path to the file

  Returns
  -------
  IM  
  Variable of class IMAGE containing at least one attribute for the image header (HEADER).
  The others attributes take their names from the objects defined in the header. 

  """

  class IMAGE:
    pass

  class ACQUISITION:
    pass

  # Read image file twice: as lines (used for the text) and as the whole (for images) --
  IM = IMAGE()
  f = open(path,'rb')
  lines = f.readlines()
  f.seek(0)
  fileContent = f.read()
  f.close()

  # HEADER in the image file --
  IM.HEADER = ''
  for L in lines:
    IM.HEADER = IM.HEADER + L.decode('ascii')
    if L.decode('ascii').strip() == 'END':
      break

  # Obtain the number of bytes and pointers needed for extracting objects --
  rpb = re.findall('RECORD_BYTES[ \t]+=[ \t]+([0-9]+)', IM.HEADER)
  RPB = int(rpb[0])
  P = re.findall('\^(\\w+)[ \t]+=[ \t]+([0-9]+)', IM.HEADER)
  pointers = [0]*len(P)
  poin = 0
  fiel = []
  for line in P:
    if not hasattr(IM, line[0]):
      fiel.append(line[0])
      pointers[poin] = int(line[1])
      poin += 1
  pointers = pointers[:poin+1]
  
  # Extract history --
  idxHist = fiel.index('HISTORY')
  setattr(IM,'HISTORY',fileContent[(pointers[idxHist]-1)*RPB:(pointers[idxHist+1]-1)*RPB].decode('ascii'))
  del pointers[idxHist]
  del fiel[idxHist]

  # Extract objects --
  ## first I need the size and the type of variable of each object
  obj = re.findall('\\bOBJECT\\b[ \t]+=[ \t]+([\\w]+)',IM.HEADER)
  width = re.findall('LINE_SAMPLES[ \t]+=[ \t]+([0-9]+)',IM.HEADER)
  height = re.findall('LINES[ \t]+=[ \t]+([0-9]+)',IM.HEADER)
  sb = re.findall('SAMPLE_BITS[ \t]+=[ \t]+([0-9]+)',IM.HEADER)

  ## now I proceed to read the whole binary, extract the selected part and convert into the correct variable type
  pointers.append(int(len(fileContent)/RPB)+1) # for defining the end of the last object
  for F in fiel:
    idx = obj.index(F)
    rawOBJ = fileContent[(pointers[idx]-1)*RPB:(pointers[idx+1]-1)*RPB]
    totSiz = int(width[idx])*int(height[idx])
    if sb[idx] == '32':
      decOBJ = np.asarray(list(struct.iter_unpack('f',rawOBJ))).reshape(int(width[idx]),int(height[idx]))
    elif sb[idx] == '8':
      decOBJ = np.asarray(list(struct.iter_unpack('B',rawOBJ))).reshape(int(width[idx]),int(height[idx]))
    setattr(IM, F, decOBJ)

  loadKernels(IM)

 # Define the ACQUISITION object with the acquisition geometry
 # NOTE: since the whole integration is done in the Cheops reference frame, I'm converting everythin into this frame
  ACQ = ACQUISITION()
 ## filter
  setattr(ACQ,'FILTER',int(re.findall('FILTER_NUMBER[ \t]+=[ \t]+"(\\d+)"\\r',IM.HEADER)[0]))
  setattr(ACQ,'PHASE_ANGLE',np.float(re.findall('PHASE_ANGLE[ \t]+=[ \t]+(.+)<deg>\\r',IM.HEADER)[0]))
 ## time 
  ST = re.findall('START_TIME[ \t]+=[ \t]+(.+)\\r',IM.HEADER)
  ET = re.findall('STOP_TIME[ \t]+=[ \t]+(.+)\\r',IM.HEADER)
  startTime = spice.str2et(ST)
  endTime = spice.str2et(ET)
  tExp = endTime - startTime
  setattr(ACQ,'START_TIME', startTime)
  setattr(ACQ,'END_TIME', endTime)
  setattr(ACQ,'EXP_TIME', tExp)
 ## binning
  binning = int(re.findall('ROSETTA:HARDWARE_BINNING_ID[ \t]+=[ \t]"([\\w]+)"\\r',IM.HEADER)[0][0])
  setattr(ACQ,'BIN', binning)
 ## FOV
  parametersFOV = spice.getfov(spice.bodn2c('ROS_OSIRIS_NAC'),10,32,32) # (instrument, max number of vectors for bounds, max number of characters for FOV shape/frame name)
  transMatrixSt = np.array(spice.pxform('ROS_OSIRIS_NAC','67P/C-G_CK',startTime))
  transMatrixEn = np.array(spice.pxform('ROS_OSIRIS_NAC','67P/C-G_CK',endTime))
  simulationToCam = np.array(spice.pxform('67P/C-G_CK','ROS_OSIRIS_NAC',startTime))
  setattr(ACQ,'TRANS_MATRIX',simulationToCam)
  boreSight = np.matmul(transMatrixSt,np.array(parametersFOV[2]))
  boreSightEnd = np.matmul(transMatrixEn,np.array(parametersFOV[2]))
  cameraRotation = boreSightEnd-boreSight
  C = (np.matmul(transMatrixSt,np.array(parametersFOV[4][i])) for i in range(parametersFOV[3])) # this is a generator object, not accessible as an array
  corners = np.empty((4,3))
  for i in range(4): corners[i,:] = next(C) # convert the corners to an array
  setattr(ACQ,'CORNERS', corners)
  # since I'm working on the non-rotating Cheops, I have to remove the rotation part in the SC velocity
  stateSC = np.array(spice.spkezr('ROS_SPACECRAFT',startTime,'67P/C-G_CK','NONE','67P/C-G')[0])[0]*1000 #output has 6 components, 3 for position and 3 for velocity, converted to meters
  omegaR = np.zeros((3))
  omegaR[-1] = 2*np.pi/(12.4*3600.)
  rotVelSC = -np.cross(omegaR,stateSC[:3])
  stateSC[3:] = stateSC[3:] - rotVelSC
  setattr(ACQ,'SC_STATE', stateSC)
  spice.kclear()
 ## heliocentric distance
  sunPos = re.findall('\\bSC_SUN_POSITION_VECTOR[ \t]+=[ \t]+\((.+)\)',IM.HEADER)[0].replace('<km>','').split(',')
  SP = np.array([float(SP) for SP in sunPos])
  setattr(ACQ,'HELIOCENTRIC_DISTANCE',np.sqrt((SP**2).sum())/1.495978707e8)

  setattr(IM,'ACQUISITION',ACQ)
  return IM

##################################################
#                                                #
#                 loadKernels                    #
#                                                #
#   This function reads the header of an image,  #
# extract the SPICE kernels needed from the      #
# header and loads them. In case the kernels are #
# not present in the local PC, it downloads them #
##################################################

def loadKernels(IM):
  """Load SPICE kernels listed in image header

  Parameters
  ----------
  image : Class IMAGE
    Variable of class IMAGE, containing at least the attribute HEADER 
    with a keyword SPICE_FILE_NAME and a list of SPICE kernels used for 
    defining the image

  Returns
  -------
  None. Loads all the kernels using the function ``furnsh`` form spicipy

  """

  import ftplib

  if not os.path.exists('SPICE_Kernels'):
    os.makedirs('SPICE_Kernels')
  presentKernels = [f for f in os.listdir('SPICE_Kernels') if os.path.isfile(os.path.join('SPICE_Kernels', f))]

  spKer = re.findall('SPICE_FILE_NAME[ \t]+=[ \t]+\((.+)\)',IM.HEADER)
  kernelList = spKer[0].replace('"','').split(',')
  for ker in kernelList:
    filNam = re.findall('(\\w+)\\\\*(\\w+\.\\w+)$',ker)[0]
    if not filNam[1] in presentKernels:
      msg = 'Kernel '+filNam[0]+'/'+filNam[1]+' not found in pool. Downloading...'
      print(msg)
      ftp_server = ftplib.FTP('spiftp.esac.esa.int')
      ftp_server.login()
      ftp_server.set_debuglevel(1)
      print(ftp_server.dir())
      impFile = 'data/SPICE/ROSETTA/kernels/'+filNam[0]+'/'+filNam[1]
      with open('SPICE_Kernels/'+filNam[1], 'wb') as fp:
        ftp_server.retrbinary('RETR '+impFile, fp.write)     
      ftp_server.close()
    spice.furnsh('SPICE_Kernels/'+filNam[1])

  return

##################################################
#                                                #
#          ray_triangle_intersection             #
#                                                #
#   Find the intersection between a segment and  #
# a triange using the Moeller–Trumbore algorithm #
##################################################

def ray_triangle_intersection(ray_start, ray_end, triangle):
  """Moeller–Trumbore intersection algorithm.

  Parameters
  ----------
  ray_start : np.ndarray
    Length three numpy array representing start point.

  ray_end : np.ndarray
    Length three numpy array representing end point.

  triangle : np.ndarray
    ``3 x 3`` numpy array containing the three vertices of a
    triangle.

  Returns
  -------
  bool
    ``True`` when there is an intersection.

  tuple
    Length three tuple containing the intersection coordinates.
    When there is no intersection, these values will be:
   ``[np.nan, np.nan, np.nan]``

  """
    
  import pyvista as pv
  # define a null intersection
  null_inter = np.array([np.nan, np.nan, np.nan])

  # break down triangle into the individual points
  v1, v2, v3 = triangle
  eps = 0.000001

  # get segment direction
  D = ray_end - ray_start

  # compute edges
  edge1 = v2 - v1
  edge2 = v3 - v1
  pvec = np.cross(D, edge2)
  det = edge1.dot(pvec)

  if abs(det) < eps:  # no intersection
    return False, null_inter
  inv_det = 1.0 / det
  tvec = ray_start - v1
  u = tvec.dot(pvec) * inv_det

  if u < 0.0 or u > 1.0:  # if not intersection
    return False, null_inter

  qvec = np.cross(tvec, edge1)
  v = D.dot(qvec) * inv_det
  if v < 0.0 or u + v > 1.0:  # if not intersection
    return False, null_inter

  t = edge2.dot(qvec) * inv_det
  if t < eps or t > 1.0:
    return False, null_inter

  intersection = (1-u-v)*v1 + u*v2 + v*v3

  return True, intersection

###################################################
#                                                 #
#                    drawTrack                    #
#                                                 #
#   Draw a track caused by a moving dust particle #
# as seen by osiris                               #
###################################################

def drawTrack(interPoint, dustVel, SCstate, tExp, corners, binning):
  """

  Parameters
  ----------
  interPoint : np.ndarray
    ``2 x 3`` numpy array representing the 2 intersection points 
    between the trajectory of the particle with the FOV

  dustVel : np.ndarray
    Length three numpy array representing particle velocity in the inertial frame.

  SCstate : np.ndarray
    Length six numpy array representing the state (position + velocity)
    of the spacecraft in the inertial frame

  tExp : float
    Exposure time of the image

  corners: np.ndarray
    ``4 x 3`` numpy array containing the FOV corners direction in the inertial frame

  binning: int
    integer representing the hardware binning used for the image acquisition

  Returns
  -------
  imageTrack : np.ndarray
    Length four numpy array representing start and endpoints of the 
    track in OSIRIS pixels

  """
  from random import random
  from shapely import geometry

# relative velocity between SC and particle
  vTotal = dustVel - SCstate[3:]

# define a random point within the trajectory
  P = interPoint[0] + random() * (interPoint[1]-interPoint[0])

# find segment endpoints
  tFuture = tExp * random()
  endP1 = P + tFuture * vTotal
  endP2 = P - (tExp-tFuture) * vTotal

# define FOV and image limits
  angularWidth = 2.208*3.14159265359/180
  W1 = angularWidth * endP1[2]
  W2 = angularWidth * endP2[2]
  FOVsize = 2048/binning
  imageLimits = geometry.Polygon([[1,1],[FOVsize,1],[FOVsize,FOVsize],[1,FOVsize]])

# find position of endpoints and define track in the image
  endP1_pix = ((endP1[:2] + W1/2)/W1)*FOVsize
  endP2_pix = ((endP2[:2] + W2/2)/W2)*FOVsize
  totalTrack = geometry.LineString([endP1_pix,endP2_pix])
  imageTrack = np.array(imageLimits.intersection(totalTrack).bounds)
  if imageTrack.size == 0:
    imageTrack = np.zeros(4,)

  return imageTrack

###################################################
#                                                 #
#                   trackBright                   #
#                                                 #
#   Find the track brightness density using the   #
# equation from Agarwal et al. 2016               # 
###################################################

def trackBright(dustSize,Rcam,binning,Filter,phAng,rh):
  """

  Parameters
  ----------
  dustSize : float
    Float representing the dust radius in meters

  IM: IMAGE class
    IMAGE class object, containing at least one IMAGE and one 
    ACQUISITION attributes, used to define the background brightness
    and the phase angle.

  Returns
  -------
  Rcam : float
    Distance between dust particle and camera (in meters)

  """
  import pandas as pd
  Flux = pd.read_csv('input/solar_flux.txt',sep='\s+',header=0)

  ## find limit brightness
  # WARNING: the image has units of W/m2/nm/SR, but the solar flux doesn't have that angular dependence
  kNAC = 3.54744e-10*np.power(binning,2) # angular size of the pixel

  ## define dust properties
  geomAlbedo = 0.04
  normConst = 0.02/np.exp(-0.045*90)
  phFun = normConst*np.exp(-0.045*phAng)

  ## find heliocentric distance and solar flux in the used filter
  solFlux = Flux.loc[Flux['Filt']==Filter].Flux.values[0]

  ## find total brigthness
  J = np.power(dustSize,2) * geomAlbedo * phFun * solFlux / np.power(rh,2) / np.power(Rcam,2) / kNAC

  return J
